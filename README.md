# PROJECT INFO

Task manager

# DELEVOPER INFO

**Name**: Fedun Alexander
**E-mail**: sasha171998@gmail.com

# SOFTWARE

- JDK 1.8
- MS Windows 10

# TECHNOLOGY STACK
- Java 
- Intellej IDEA 2020.2 Ultimate
- Git

# PROGRAM BUILD

- Create folder "bin"
- 
```bush
javac -d bin .\src\ru\fedun\tm\*
```
-
```bush
jar -cmf .\resources\META-INF\MANIFEST.MF task-manager.jar -C bin .
```

# PROGRAM RUN 
```bash
java -jar ./task-manager-03.jar
```

# SCREENSHOTS
https://yadi.sk/d/a01T62FI5-ADtQ?w=1