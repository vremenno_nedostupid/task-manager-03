package ru.fedun.tm;

import static ru.fedun.tm.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        System.out.println("Welcome to task manager");
        parseArgs(args);
    }

    private static void parseArgs(final String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        if (param.isEmpty()) return;
        switch (param) {
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
        }
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.exit(0);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Alexander Fedun");
        System.out.println("sasha171998@gmail.com");
        System.exit(0);
    }

}
