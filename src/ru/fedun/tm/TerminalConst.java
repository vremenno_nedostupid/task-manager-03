package ru.fedun.tm;

public interface TerminalConst {

    String CMD_HELP = "help";

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

}
